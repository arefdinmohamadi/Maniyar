//
//  UsersModel.swift
//  maniyarSmapleFor-ios
//
//  Created by ArefDInmohamadi on 4/9/18.
//  Copyright © 2018 ArefDInmohamadi. All rights reserved.
//

import Foundation
import UIKit

class  UsersModel {
    var name : String?
    var id : Int64?
    var phoneNumber : String?
    var picture : UIImage?
    
    init() {
        
    }
    init(id : Int64? , name : String? , phoneNumber : String? , picture : UIImage?) {
        self.name = name
        self.id = id
        self.phoneNumber = phoneNumber
        self.picture = picture
    }
}
