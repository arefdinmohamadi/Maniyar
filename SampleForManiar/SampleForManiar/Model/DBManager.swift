//
//  DBManager.swift
//  maniyarSmapleFor-ios
//
//  Created by ArefDInmohamadi on 4/9/18.
//  Copyright © 2018 ArefDInmohamadi. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class DBManager {
    
    private func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    // Select , Update , Insert , Delete
    
    // A: Insert
    func insertNewCase(newEntity : UsersModel , withCompletionHandler : @escaping (Bool,NSError?)->Void) {
        //1: context baraye etesal be CDB
        let context = self.getContext()
        // 2: hala oon entity KHAS mad nazare mast
        let newCase = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context)
        //3 : hamsansazi bayad anjam shavad beyne entity asl man va classe modelam
        newCase.setValue(newEntity.name, forKey: "name")
        newCase.setValue(newEntity.id, forKey: "id")
        newCase.setValue(newEntity.phoneNumber, forKey: "phoneNumber")
        newCase.setValue(nil, forKey: "picture")
        
        //saving...
        do{
            try context.save()
            withCompletionHandler(true, nil)
            
            print("Saved")
        }catch let error {
            withCompletionHandler(false, error as NSError)
            print(error)
        }
    }
    
    // B: SELECT        **** baraye namayesh dar table ya har logie ****
    func selectAllCases(withComplitionHandler: @escaping ([UsersModel]?,NSError?)->Void){
        let context = self.getContext()
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Users")
        req.returnsObjectsAsFaults = false
        do{
            let results = try context.fetch(req) as! [NSManagedObject]
            var finalResult = [UsersModel]()
            for item in results {
                let objs = UsersModel()
                objs.name = item.value(forKey: "name") as? String
                objs.picture = item.value(forKey: "picture") as? UIImage
                objs.id = item.value(forKey: "id") as? Int64
                objs.phoneNumber = item.value(forKey: "phoneNumber") as? String
                finalResult.append( objs)
            }
            withComplitionHandler(finalResult, nil)
        }catch let error {
            print("Error \(error)")
            withComplitionHandler(nil, error as NSError)
        }
    }
    
    //C: UPADTE **************************
    func updateCase (newObj:UsersModel , withComplitionHandler:@escaping (Bool , NSError?)->Void) {
        let context = getContext()
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Users")
        req.returnsObjectsAsFaults = false
        do{
            let result = try context.fetch(req) as! [NSManagedObject]
            for item in result {
                if newObj.id == item.value(forKey: "id") as? Int64 {
                    item.setValue(newObj.name, forKey: "name")
                    item.setValue(nil, forKey: "name")
                    item.setValue(newObj.phoneNumber, forKey: "phoneNumber")
                    item.setValue(newObj.id, forKey: "id")
                    
                    do{
                        try context.save()
                        withComplitionHandler(true, nil)
                        
                        print("Saved")
                    }catch let error {
                        withComplitionHandler(false, error as NSError)
                        print(error)
                    }
                }
            }
        }catch let error {
            withComplitionHandler(false, error as NSError)
            print(error)
        }
    }
    // d: DELETE  ************************
    func deleteCase(entityID : Int64 , withComplitionHandler : @escaping (Bool,NSError?)->Void) {
        let context = getContext()
        self.selectAllCases { (info, error) in
            if info != nil , error == nil {
                let req = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
                req.returnsObjectsAsFaults = false
                do {
                    let results  = try context.fetch(req) as! [NSManagedObject]
                    for item in results {
                        if entityID == item.value(forKey: "id") as! Int64 {
                            context.delete(item)
                        }
                    }
                    do{
                        try context.save()
                        withComplitionHandler(true, nil)
                    }catch let error {
                        withComplitionHandler(false, error as NSError)
                    }
                }catch let error {
                    withComplitionHandler(false, error as NSError)
                }
            }
        }
    }
    
    
}

