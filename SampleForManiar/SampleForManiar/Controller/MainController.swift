//
//  MainController.swift
//  maniyarSmapleFor-ios
//
//  Created by ArefDInmohamadi on 4/9/18.
//  Copyright © 2018 ArefDInmohamadi. All rights reserved.
//

import UIKit

class MainController: UIViewController , UITableViewDataSource , UITableViewDelegate , UICollectionViewDataSource , UICollectionViewDelegate , UISearchBarDelegate {
    
    var userInfo = [UsersModel]()
    
    var likedUsers = [UsersModel]()
    
    var likeOrDisLike : [Bool] = []
    
    var allUsers = [UsersModel]()
    
    static var sendLiked = [UsersModel]()
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.fetchAllUsers()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func likeAndUnLike(_ sender: UIButton) {
        //TableView
        
        self.likeOrDisLike[sender.tag] = !likeOrDisLike[sender.tag]
        let indexPath = IndexPath(item: sender.tag, section: 0)
        tblView.reloadRows(at: [indexPath], with: .automatic)
         self.likeManager()
    }
    
    @IBAction func disLikeClicked(_ sender: UIButton) {
        //CollectionView
        
        self.likeOrDisLike[sender.tag] = false
        let indexPath = IndexPath(item: sender.tag, section: 0)
        tblView.reloadRows(at: [indexPath], with: .automatic)
        self.likeManager()
    }
    
    
    
    // ViewControll :
    //tableView Config
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userInfo.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        cell.imgView?.layer.cornerRadius = 25
        cell.imgView?.clipsToBounds = true
        cell.selectionStyle = .none
        
        cell.lblName.text = self.userInfo[indexPath.row].name
        cell.lblPhoneNumber.text = self.userInfo[indexPath.row].phoneNumber
        cell.btnLikeDisLike.tag = indexPath.row
        
        if self.likeOrDisLike[indexPath.row] {
            cell.btnLikeDisLike.setImage(#imageLiteral(resourceName: "tik"), for: .normal)
        }else{
            cell.btnLikeDisLike.setImage(#imageLiteral(resourceName: "khali"), for: .normal)
        }
        
        if self.userInfo[indexPath.row].picture == nil {
            cell.imgView.image = #imageLiteral(resourceName: "personal")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    //CollectionView Config
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.likedUsers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        cell.imgView.layer.borderColor = (UIColor.black).cgColor
        cell.imgView.layer.borderWidth = 1
        cell.imgView.layer.cornerRadius = 30
        cell.imgView.clipsToBounds = true
        
        cell.btnDislike.tag = indexPath.row
        
        cell.lblPhoneNumber.text = self.likedUsers[indexPath.row].name
        if self.likedUsers[indexPath.row].picture == nil {
            cell.imgView.image = #imageLiteral(resourceName: "personal")
        }
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            self.userInfo = self.allUsers
        }else{
            self.userInfo = self.allUsers.filter({ (thisUser) -> Bool in
                return (thisUser.name?.contains(searchBar.text!))! || (thisUser.phoneNumber?.contains(searchBar.text!))!
            })
        }
        self.tblView.reloadData()
    }
    
    // Buttons
    @IBAction func okClicked(_ sender: UIButton) {
        MainController.sendLiked = self.likedUsers
        
        let page = self.storyboard?.instantiateViewController(withIdentifier: "DetailController") as! DetailController
        self.navigationController?.pushViewController(page, animated: true)
    }
    
    
    @IBAction func backClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //DB
    
    //Fetch
    func fetchAllUsers () {
        let manager = DBManager.init()
        manager.selectAllCases { (info, error) in
            if info != nil , error == nil {
                self.allUsers = info!
                self.userInfo = self.allUsers
                for _ in self.allUsers {
                    self.likeOrDisLike.append(false)
                }
                self.tblView.reloadData()
            }else{
                print("error darim")
            }
        }
    }
    
    
    func likeManager () {
         self.likedUsers = []
        
        for index in 1...self.likeOrDisLike.count {
            
           
            
            if self.likeOrDisLike[index-1] == true {
                
                self.likedUsers.append(self.userInfo[index-1])
            }
            
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.tblView.reloadData()
        }
       
    }
}
