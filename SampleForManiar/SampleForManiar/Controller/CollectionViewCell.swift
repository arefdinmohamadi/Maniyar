//
//  CollectionViewCell.swift
//  maniyarSmapleFor-ios
//
//  Created by ArefDInmohamadi on 4/9/18.
//  Copyright © 2018 ArefDInmohamadi. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnDislike: UIButton!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
}
