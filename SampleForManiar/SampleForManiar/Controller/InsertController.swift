//
//  InsertController.swift
//  maniyarSmapleFor-ios
//
//  Created by ArefDInmohamadi on 4/9/18.
//  Copyright © 2018 ArefDInmohamadi. All rights reserved.
//

import UIKit

class InsertController: UIViewController {

    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func insertClicked(_ sender: UIButton) {
        if self.txtName.text != nil , self.txtPhoneNumber.text != nil {
            let manager = DBManager.init()
            manager.insertNewCase(newEntity: UsersModel.init(id: 0, name: self.txtName.text!, phoneNumber: self.txtPhoneNumber.text!, picture: nil), withCompletionHandler: { (info, error) in
                if error == nil {
                    self.txtName.text = ""
                    self.txtPhoneNumber.text = ""
                    
                    print("succeded")
                }else{
                    print("error is : \(error)")
                }
            })
        }else{
            let alert = UIAlertController(title: "خطا", message: "فیلد خالیست!", preferredStyle: .alert)
            let action = UIAlertAction(title: "باشه", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func nextClicked(_ sender: UIButton) {
        let page = self.storyboard?.instantiateViewController(withIdentifier: "MainController") as! MainController
        self.navigationController?.pushViewController(page, animated: true)
    }
    
}
